<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Order</title>
</head>
<body>

<form:form method="post" action="add" commandName="order">
	Parts:
	<select multiple="multiple" name="partId">
		<c:forEach items="${parts}" var="part">
		<option value="${part.partId}">${part.name}</option>
		</c:forEach>
	</select><br/>
	Amount: <input name="amount" type="text" /><br/>
	<select name="clientId">
		<c:forEach items="${clients}" var="client">
		<option value="${client.clientId}">${client.name}</option>
		</c:forEach>
	</select>
	<br/>
	<button>Add order</button>
</form:form>

            <c:if test="${!empty orders}">
                <h3>Orders</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Client</th>
                        <th>Amount</th>
                        <th>Parts</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${orders}" var="order">
                        <tr>
                            <td>${order.client.name}</td>
                            <td>${order.amount}</td>
                            <td>
                                <c:forEach items="${order.parts}" var="part">
                                    ${part.name}
                                </c:forEach>
                            </td>
                            <td>
                                <a href="json/${order.orderId}">JSON</a>
				<a href="delete/${order.orderId}">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

</body>
</html>
