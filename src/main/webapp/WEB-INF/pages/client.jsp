<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Client</title>
</head>
<body>

<form:form method="post" action="add" commandName="client">
	Name: <input name="name" type="text" /><br/>
	Address: <input name="address" type="text" /><br/>
	NIP: <input name="nip" type="text" /><br/>
	<button>Add client</button>
</form:form>

            <c:if test="${!empty clients}">
                <h3>Client</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>NIP</th>
						<th>Orders</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${clients}" var="client">
                        <tr>
                            <td>${client.name}</td>
                            <td>${client.address}</td>
                            <td>${client.nip}</td>
                            <td>
                            	<c:forEach items="${client.orders}" var="order">
                            	${order.orderId}<br/>
                            	</c:forEach>
                            </td>
                            <td>
                                <a href="json/${client.name}">JSON</a>
                                <a href="delete/${client.clientId}">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

</body>
</html>
