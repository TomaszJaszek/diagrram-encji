<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Part</title>
</head>
<body>

<form:form method="post" action="add" commandName="client">
	Name: <input name="name" type="text" /><br/>
	Price: <input name="price" type="text" /><br/>
	Description: <input name="description" type="text" /><br/>
	<button>Add part</button>
</form:form>

            <c:if test="${!empty parts}">
                <h3>Parts</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${parts}" var="part">
                        <tr>
                            <td>${part.name}</td>
                            <td>${part.price}</td>
                            <td>${part.description}</td>
                            <td>
                                <a href="json/${part.name}">JSON</a>
                                <a href="delete/${part.partId}">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>

</body>
</html>
