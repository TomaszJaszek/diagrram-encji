package com.hurtownia.Hurtownia.model.service;

import java.util.List;

import com.hurtownia.Hurtownia.model.domain.Order;
import com.hurtownia.Hurtownia.util.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {

    public List<Order> readAll();

}
