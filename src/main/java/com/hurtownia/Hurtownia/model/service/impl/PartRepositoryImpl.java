package com.hurtownia.Hurtownia.model.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hurtownia.Hurtownia.model.domain.Part;
import com.hurtownia.Hurtownia.model.service.PartRepository;
import com.hurtownia.Hurtownia.util.CustomHibernateDaoSupport;


@Service("partRepository")
@Transactional
public class PartRepositoryImpl extends CustomHibernateDaoSupport implements PartRepository {

	public void create(Part c) {
		getHibernateTemplate().save(c);
	}

	public Part read(Long id) {
		return (Part) getHibernateTemplate().get(Part.class, id);
	}

	public void update(Part c) {
		getHibernateTemplate().update(c);
	}

	public void delete(Part c) {
		getHibernateTemplate().delete(c);
	}

	public List<Part> readAll() {
		return getHibernateTemplate().find("from Part");
	}

	public Part readByName(String name) {
		return (Part) getHibernateTemplate().find("from Part where name = ?", name).get(0);
	}

}
