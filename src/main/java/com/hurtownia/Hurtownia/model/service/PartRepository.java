package com.hurtownia.Hurtownia.model.service;

import java.util.List;

import com.hurtownia.Hurtownia.model.domain.Part;
import com.hurtownia.Hurtownia.util.CrudRepository;

public interface PartRepository extends CrudRepository<Part, Long> {

    public List<Part> readAll();

    public Part readByName(String name);

}
