package com.hurtownia.Hurtownia.model.service;

import java.util.List;

import com.hurtownia.Hurtownia.model.domain.Client;
import com.hurtownia.Hurtownia.util.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
	public List<Client> readAll();
	public Client readByName(String name);
}
