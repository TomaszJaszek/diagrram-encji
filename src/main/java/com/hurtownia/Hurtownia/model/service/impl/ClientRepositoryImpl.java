package com.hurtownia.Hurtownia.model.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hurtownia.Hurtownia.model.domain.Client;
import com.hurtownia.Hurtownia.model.service.ClientRepository;
import com.hurtownia.Hurtownia.util.CustomHibernateDaoSupport;

@Service("clientRepository")
@Transactional
public class ClientRepositoryImpl extends CustomHibernateDaoSupport implements ClientRepository {

        @Override
	public void create(Client c) {
		getHibernateTemplate().save(c);
	}

        @Override
	public Client read(Long id) {
		return (Client) getHibernateTemplate().get(Client.class, id);
	}

        @Override
	public void update(Client c) {
		getHibernateTemplate().update(c);
	}

        @Override
	public void delete(Client c) {
		getHibernateTemplate().delete(c);
	}

        @Override
	public List<Client> readAll() {
		return getHibernateTemplate().find("from Client");
	}

        @Override
	public Client readByName(String name) {
		return (Client) getHibernateTemplate().find("from Client where name = ?", name).get(0);
	}

}
