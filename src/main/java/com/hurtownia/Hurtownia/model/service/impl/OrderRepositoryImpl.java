package com.hurtownia.Hurtownia.model.service.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hurtownia.Hurtownia.model.domain.Order;
import com.hurtownia.Hurtownia.model.service.OrderRepository;
import com.hurtownia.Hurtownia.util.CustomHibernateDaoSupport;

@Service("orderRepository")
@Transactional
public class OrderRepositoryImpl extends CustomHibernateDaoSupport implements OrderRepository {

	public void create(Order c) {
		getHibernateTemplate().save(c);
	}

	public Order read(Long id) {
		return (Order) getHibernateTemplate().get(Order.class, id);
	}

	public void update(Order c) {
		getHibernateTemplate().update(c);
	}

	public void delete(Order c) {
		getHibernateTemplate().delete(c);
	}

	public List<Order> readAll() {
		return getHibernateTemplate().find("from Order");
	}

}
