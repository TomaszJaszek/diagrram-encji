package com.hurtownia.Hurtownia.model.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "Client")
public class Client implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long clientId;
	@Column(unique = true)
	private int nip;
	@Column(nullable = false)
	private String address;
	@Column(nullable = false, unique = true)
	private String name;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "client")
	private Set<Order> orders;
	
	public Client() {
		super();
	}
	public long getClientId() {
		return clientId;
	}
	public void setClientId(long clientId) {
		this.clientId = clientId;
	}
	public int getNip() {
		return nip;
	}
	public void setNip(int nip) {
		this.nip = nip;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Order> getOrders() {
		return orders;
	}
	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
	
	@Override
	public String toString() {
		return "Client [clientId=" + clientId + ", nip=" + nip + ", address="
				+ address + ", name=" + name + "]";
	}
	
}
