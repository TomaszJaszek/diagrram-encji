package com.hurtownia.Hurtownia.model.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "Orders")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;
    @Column(nullable = false)
    private int amount;

    @ManyToOne(fetch = FetchType.EAGER)
    private Client client;

    private String info;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Part> parts;

    public Order() {
        super();
    }

    public Order(long orderId, int amount, Client client, String info,
            Set<Part> parts) {
        super();
        this.orderId = orderId;
        this.amount = amount;
        this.client = client;
        this.info = info;
        this.parts = parts;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Set<Part> getParts() {
        return parts;
    }

    public void setParts(Set<Part> parts) {
        this.parts = parts;
    }

    @Override
    public String toString() {
        return "Order [orderId=" + orderId + ", amount=" + amount + ", client="
                + client + ", info=" + info + ", parts=" + parts + "]";
    }

}
