package com.hurtownia.Hurtownia.model.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "Part")
public class Part implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long partId;
	@Column(nullable = false, unique = true)
	private String name;
	@Column(nullable = false)
	private double price;
	private String description;
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Order order;
	
	public Part() {
		super();
	}

	public long getPartId() {
		return partId;
	}

	public void setPartId(long partId) {
		this.partId = partId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
}
