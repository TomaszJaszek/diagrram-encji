package com.hurtownia.Hurtownia.util;

@SuppressWarnings("hiding")
public interface CrudRepository <Class, Serializable> {
	public void create(Class c);
	public Class read(Serializable id);
	public void update(Class c);
	public void delete(Class c);
}
