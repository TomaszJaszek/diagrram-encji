package com.hurtownia.Hurtownia.controller;

import com.hurtownia.Hurtownia.model.domain.Client;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hurtownia.Hurtownia.model.domain.Order;
import com.hurtownia.Hurtownia.model.domain.Part;
import com.hurtownia.Hurtownia.model.service.ClientRepository;
import com.hurtownia.Hurtownia.model.service.OrderRepository;
import com.hurtownia.Hurtownia.model.service.PartRepository;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private PartRepository partRepository;

    @RequestMapping({"", "/"})
    public String showOrders(ModelMap model) {
        model.addAttribute("order", new Order());
        model.addAttribute("orders", orderRepository.readAll());
        model.addAttribute("clients", clientRepository.readAll());
        model.addAttribute("parts", partRepository.readAll());
        return "order";
    }

    @RequestMapping(value = {"/add", "add"}, method = RequestMethod.POST)
    public String createOrder(@ModelAttribute Order order, @RequestParam String[] partId, @RequestParam long clientId) {
        order.setClient(clientRepository.read(clientId));
        Set<Part> parts = new HashSet<Part>();
        for (String pId : partId) {
            parts.add(partRepository.read(Long.parseLong(pId)));
        }
        order.setParts(parts);
        orderRepository.create(order);
        return "redirect:/order/";
    }

    @RequestMapping({"/delete/{orderId}", "delete/{orderId}"})
    public String deleteOrder(@PathVariable("orderId") long orderId) {
        orderRepository.delete(orderRepository.read(orderId));
        return "redirect:/order/";
    }

    @RequestMapping("/json")
    public @ResponseBody
    String readAllJson() {
        JSONArray orderArray = new JSONArray();
        for (Order order : orderRepository.readAll()) {
            JSONObject orderJSON = new JSONObject();
            orderJSON.put("orderId", order.getOrderId());
            orderJSON.put("amount", order.getAmount());
            orderJSON.put("info", order.getInfo());
            orderArray.put(orderJSON);
        }
        return orderArray.toString();
    }

    @RequestMapping("/json/{orderId}")
    public @ResponseBody String readAllJson(@PathVariable long orderId) {
        JSONArray orderArray = new JSONArray();
        Order order = orderRepository.read(orderId);
        JSONObject orderJSON = new JSONObject();
        orderJSON.put("orderId", order.getOrderId());
        orderJSON.put("amount", order.getAmount());
        orderJSON.put("info", order.getInfo());
        orderArray.put(orderJSON);
        return orderArray.toString();
    }

}
