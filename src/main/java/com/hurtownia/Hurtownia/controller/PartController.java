package com.hurtownia.Hurtownia.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hurtownia.Hurtownia.model.domain.Part;
import com.hurtownia.Hurtownia.model.service.PartRepository;

@Controller
@RequestMapping("/part")
public class PartController {

	@Autowired
	private PartRepository partRepository;

	@RequestMapping({ "", "/" })
	public String manageClients(ModelMap model) {
		model.addAttribute("part", new Part());
		model.addAttribute("parts", partRepository.readAll());
		return "part";
	}

	@RequestMapping(value = { "/add", "add", "/add/", "add/" }, method = RequestMethod.POST)
	public String createClient(@ModelAttribute Part part) {
		partRepository.create(part);
		return "redirect:/part/";
	}

	@RequestMapping({ "/delete/{partId}", "delete/{partId}" })
	public String deleteClient(@PathVariable("partId") long partId) {
		partRepository.delete(partRepository.read(partId));
		return "redirect:/part/";
	}

	@RequestMapping({ "json", "/json" })
	public @ResponseBody String readAllJson() {
		JSONArray partArray = new JSONArray();
		for (Part part : partRepository.readAll()) {
			JSONObject clientJSON = new JSONObject();
			clientJSON.put("partId", part.getPartId());
			clientJSON.put("name", part.getName());
			clientJSON.put("price", part.getPrice());
			partArray.put(clientJSON);
		}
		return partArray.toString();
	}

	@RequestMapping({ "json/{name}", "/json/{name}" })
	public @ResponseBody String readAllJson(@PathVariable("name") String name) {
		JSONArray partArray = new JSONArray();
		Part part = partRepository.readByName(name);
		JSONObject clientJSON = new JSONObject();
		clientJSON.put("partId", part.getPartId());
		clientJSON.put("name", part.getName());
		clientJSON.put("price", part.getPrice());
		partArray.put(clientJSON);
		return partArray.toString();
	}
}
