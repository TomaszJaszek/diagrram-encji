package com.hurtownia.Hurtownia.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hurtownia.Hurtownia.model.domain.Client;
import com.hurtownia.Hurtownia.model.domain.Order;
import com.hurtownia.Hurtownia.model.service.ClientRepository;
import com.hurtownia.Hurtownia.model.service.OrderRepository;

@Controller
@RequestMapping("/client")
public class ClientController {
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@RequestMapping({"","/"})
	public String manageClients(ModelMap model) {
		model.addAttribute("client", new Client());
		model.addAttribute("clients", clientRepository.readAll());
		return "client";
	}
	
	@RequestMapping(value = {"/add","add","/add/","add/"}, method = RequestMethod.POST)
	public String createClient(@ModelAttribute Client client) {
		clientRepository.create(client);
		return "redirect:/client/";
	}
	
	@RequestMapping({"/delete/{clientId}","delete/{clientId}"})
	public String deleteClient(@PathVariable("clientId") long clientId) {
		clientRepository.delete(clientRepository.read(clientId));
		return "redirect:/client/";
	}
	
	@RequestMapping({"json","/json"})
	public @ResponseBody String readAllJson() {
            JSONArray clientArray = new JSONArray();
            for (Client client : clientRepository.readAll()) {
                JSONObject clientJSON = new JSONObject();
                clientJSON.put("clientId", client.getClientId());
                clientJSON.put("name", client.getName());
                clientJSON.put("address", client.getAddress());
                clientJSON.put("nip", client.getNip());
                JSONObject orderJSON = new JSONObject();
                for (Order order : client.getOrders()) {
                    orderJSON.put("orderId", order.getOrderId());
                    orderJSON.put("amount", order.getAmount());
                    orderJSON.put("info", order.getInfo());
                }
                clientJSON.put("orders", orderJSON);
                clientArray.put(clientJSON);
            }
            return clientArray.toString();
	}
	
	@RequestMapping({"json/{name}","/json/{name}"})
	public @ResponseBody String readAllJson(@PathVariable("name") String name) {
            JSONArray clientArray = new JSONArray();
            Client client = clientRepository.readByName(name);
            JSONObject clientJSON = new JSONObject();
            clientJSON.put("clientId", client.getClientId());
            clientJSON.put("name", client.getName());
            clientJSON.put("address", client.getAddress());
            clientJSON.put("nip", client.getNip());
            JSONObject orderJSON = new JSONObject();
            for (Order order : client.getOrders()) {
                orderJSON.put("orderId", order.getOrderId());
                orderJSON.put("amount", order.getAmount());
                orderJSON.put("info", order.getInfo());
            }
            clientJSON.put("orders", orderJSON);
            clientArray.put(clientJSON);
            return clientArray.toString();
	}
}
