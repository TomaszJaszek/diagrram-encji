package com.hurtownia.Hurtownia.model.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class ClientRepositoryTest extends TestCase {

    private final String JDBC = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String CLIENT_NAME = "Jakieś imie";

    @Autowired
    private ClientRepository clientRepository;

    @Before
    public void setUp() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        stmt.execute("INSERT INTO Client (address, name, nip) VALUES ('Adres klienta','Nazwa/Imie Klienta', 1234567890)");
        stmt.execute("INSERT INTO Client (address, name, nip) VALUES ('Adres klienta','" + CLIENT_NAME + "', 987654321)");
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        stmt.execute("TRUNCATE TABLE Client");
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Client");
        int result = 0;
        while (rs.next()) {
            result++;
        }
        if (null != rs) {
            rs.close();
        }
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
        assertEquals(result, clientRepository.readAll().size());
    }

    @Test
    public void testReadByName() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT name FROM Client where name = '" + CLIENT_NAME + "'");
        String result = "";
        if (rs.next()) {
            result = rs.getString("name");
        }
        if (null != rs) {
            rs.close();
        }
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
        assertEquals(result, clientRepository.readByName(CLIENT_NAME).getName());
    }

}
