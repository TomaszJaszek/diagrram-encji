package com.hurtownia.Hurtownia.model.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import junit.framework.TestCase;
import static junit.framework.TestCase.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class OrderRepositoryTest extends TestCase {
    
    private final String JDBC = "jdbc:hsqldb:hsql://localhost/workdb";
    
    private final String ORDER_INFO = "Uwagi do zamówienia";
    
    	@Autowired
	private OrderRepository orderRepository;
	
	@Before
	public void setUp() throws Exception {
		Connection conn = DriverManager.getConnection(JDBC);
		Statement stmt = conn.createStatement();
                stmt.execute("INSERT INTO Orders (amount, info) VALUES (10,'Byle jaka uwaga')");
                stmt.execute("INSERT INTO Orders (amount, info) VALUES (20,'"+ ORDER_INFO +"')");
                if (null != stmt)
			stmt.close();
		if (null != conn)
			conn.close();
	}
	
	@After
	public void tearDown() throws Exception {
		Connection conn = DriverManager.getConnection(JDBC);
		Statement stmt = conn.createStatement();
		stmt.execute("TRUNCATE TABLE Orders");
		if (null != stmt)
			stmt.close();
		if (null != conn)
			conn.close();
	}
	
	@Test
	public void testReadAll() throws Exception {
		Connection conn = DriverManager.getConnection(JDBC);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM Orders");
		int result = 0;
		while (rs.next())
			result++;
		if (null != rs) 
			rs.close();
		if (null != stmt)
			stmt.close();
		if (null != conn)
			conn.close();
		assertEquals(result, orderRepository.readAll().size());
	}

    
}
