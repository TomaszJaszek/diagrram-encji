package com.hurtownia.Hurtownia.model.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import junit.framework.TestCase;
import static junit.framework.TestCase.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class PartRepositoryTest extends TestCase {

    private final String JDBC = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String PART_NAME = "Jakaś nazwa";

    @Autowired
    private PartRepository partRepository;

    @Before
    public void setUp() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        stmt.execute("INSERT INTO Part (name, price, description) VALUES ('Część',11.99, 'Opis produktu')");
        stmt.execute("INSERT INTO Part (name, price, description) VALUES ('" + PART_NAME + "', 17.9, 'Drugi opis produktu')");
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
    }

    @After
    public void tearDown() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        stmt.execute("TRUNCATE TABLE Part");
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
    }

    @Test
    public void testReadAll() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Part");
        int result = 0;
        while (rs.next()) {
            result++;
        }
        if (null != rs) {
            rs.close();
        }
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
        assertEquals(result, partRepository.readAll().size());
    }

    @Test
    public void testReadByName() throws Exception {
        Connection conn = DriverManager.getConnection(JDBC);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT name FROM Part where name = '" + PART_NAME + "'");
        String result = "";
        if (rs.next()) {
            result = rs.getString("name");
        }
        if (null != rs) {
            rs.close();
        }
        if (null != stmt) {
            stmt.close();
        }
        if (null != conn) {
            conn.close();
        }
        assertEquals(result, partRepository.readByName(PART_NAME).getName());
    }
}
